﻿using UnityEngine;
using System.Collections;

public class Null : MonoBehaviour {
    NullZones _zones;
    NullZones zones { get { return _zones = _zones ?? GetComponentInParent<NullZones>(); } }
    public void OnEnable()
    {
        zones.register(this);
        bounds = transform.GetComponentInChildren<Collider2D>().bounds;
    }
    public Bounds bounds { get; private set; }
}
