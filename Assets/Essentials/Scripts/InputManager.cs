﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {
    public static InputManager instance;
	public bool gotInput { get { return input != InputType.none; } }
    public bool tryEatInput() { InputType it; return tryEatInput(out it); }
    public bool tryEatInput(out InputType it)
    {
        it = InputType.none;
        if(gotInput)
        {
            input = InputType.none;
            return true;
        }
        return false;
    }
    InputType input = InputType.none;
    public enum InputType
    {
        next, none
    }
    void Awake () {
        instance = this;
	}

    float mouseDownAt = -1;
    Vector3 mouseDownV3 = Vector3.zero;
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            input = InputType.next;
        }
        if(Input.GetMouseButtonDown(0))
        {
            mouseDownAt = Time.time;
            mouseDownV3 = Input.mousePosition;
        }
        if(Input.GetMouseButtonUp(0))
        {
            if (Time.time - mouseDownAt < 0.5f && (mouseDownV3 - Input.mousePosition).magnitude < 100f)
            {
                input = InputType.next;
            }
        }
    }















    #region UnityUI
    public void RedoLevel()
    {
        //GameManager.LevelLost();
    }
    public void PrevLevel()
    {
        //GameManager.currentLevel--;
    }
    public void NextLevel()
    {
        //GameManager.currentLevel++;
    }
    #endregion
}
