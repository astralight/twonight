﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof(BoxCollider))]
public abstract class BoundedCollidable : Collidable {
    public Bounds bounds;
    public override void Init()
    {
        bounds = GetComponent<BoxCollider>().bounds;
    }
    public override bool canCollide(Vector3 otherPosition)
    {
        bool b = (base.canCollide(otherPosition) || bounds.Contains(otherPosition));
        return b;
    }
}
