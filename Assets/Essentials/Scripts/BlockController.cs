﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockController : MonoBehaviour
{
    [SerializeField]
    float XSPEED = 100, YSPEED = 100;
    [SerializeField]
    float GHOST_NULL_ZONE_MULTIPLIER = 0.5f;
    private GameObject _bounds = null;
    private GameObject _xBound = null;
    private GameObject xBound
    {
        get
        {
            return _xBound == null ? (_xBound = bounds.transform.FindChild("X").gameObject) : _xBound;
        }
    }
    private GameObject _yBound = null;
    private GameObject yBound
    {
        get
        {
            return _yBound == null ? (_yBound = bounds.transform.FindChild("Y").gameObject) : _yBound;
        }
    }
    public GameObject bounds
    {
        get
        {
            return _bounds == null ? (_bounds = transform.FindChild("Bounds").gameObject) : _bounds;
        }
    }
    private GameObject _blockContainer = null;
    public GameObject blockContainer
    {
        get
        {
            return _blockContainer == null ? (_blockContainer = transform.parent.FindChild("BlockContainer").gameObject) : _blockContainer;
        }
    }
    public GameObject ghost;
    Color nullColor = new Color(0, 0, 0, 0.2f);
    Vector2 maxBounds = new Vector2(20 - 1, 10 - 1);
    [SerializeField] Material xMat, yMat;
    void Begin()
    {
        StartCoroutine(BeginIE());
        xMat = transform.FindChild("Bounds").FindChild("X").GetComponent<Renderer>().material;
        yMat = transform.FindChild("Bounds").FindChild("Y").GetComponent<Renderer>().material;
    }
    IEnumerator BeginIE()
    {
        transform.localScale = blockContainer.transform.localScale;
        foreach (Transform t in blockContainer.transform)
            Align(t);
        yield return null;
        while ((ghost = NextBlock()) != null)
        {
            InputManager.instance.tryEatInput();
            UI.Blocks.instance.SetBlocks(blocks);
            yBound.transform.localPosition = xBound.transform.localPosition = Vector3.zero;
            bool dirNegX = true;
            bool dirNegY = true;
            float STEP = 0.001f;
            Transform ghostT = Instantiate(ghost).transform; //Transform of ghost
            ghostT.parent = transform;
            ghostT.localPosition = Vector3.zero;
            Transform ghostN = Instantiate(ghost).transform; //Null zone of ghost
            ghostT.gameObject.name = "GhostT";
            ghostN.gameObject.name = "GhostN";
            ghostN.parent = transform;
            ghostT.localScale = ghostN.localScale = Vector3.one;
            ghostN.localPosition = Vector3.zero;
            //ghostN.localScale *= 1 + GHOST_NULL_ZONE_MULTIPLIER;
            ghostT.up = ghostN.up = -Vector3.forward;

            PolygonCollider2D poly = ghostN.GetComponentInChildren<PolygonCollider2D>();
            {
                Rigidbody2D rb2d = poly.gameObject.AddComponent<Rigidbody2D>();
                rb2d.gravityScale = 0;
                rb2d.freezeRotation = true;
                poly.isTrigger = true;
            }

            SetLayerRecursively(ghostT, "Ghost");
            SetLayerRecursively(ghostN, "Ghost");
            SpriteRenderer sr;
            Color color = (sr = ghostT.gameObject.GetComponentInChildren<SpriteRenderer>()).material.color;
            sr.material.color = nullColor; // Color.Lerp(Color.gray, Color.black, 0.5f);
            ghostN.gameObject.GetComponentInChildren<SpriteRenderer>().material.color = nullColor;

            bool validSpot = false;
            while(!validSpot)
            {
                //X
                dirNegX = true;
                while (!InputManager.instance.tryEatInput())
                {
                    yBound.transform.Translate(Vector3.right * STEP * YSPEED * (dirNegX ? -1 : 1));
                    if (yBound.transform.localPosition.x < -maxBounds.x)
                        dirNegX = false;
                    if (yBound.transform.localPosition.x > maxBounds.x)
                        dirNegX = true;
                    ghostN.localPosition = ghostT.localPosition = new Vector3(yBound.transform.localPosition.x, xBound.transform.localPosition.y, 0);
                    yield return new WaitForSeconds(0.01f);
                }
                //Y
                dirNegY = true;
                while (!InputManager.instance.tryEatInput())
                {
                    xBound.transform.Translate(Vector3.up * STEP * XSPEED * (dirNegY ? -1 : 1));
                    if (xBound.transform.localPosition.y < -maxBounds.y)
                        dirNegY = false;
                    if (xBound.transform.localPosition.y > maxBounds.y)
                        dirNegY = true;
                    ghostN.localPosition = ghostT.localPosition = new Vector3(yBound.transform.localPosition.x, xBound.transform.localPosition.y, 0);
                    yield return new WaitForSeconds(0.01f);
                }

                //Make sure this isn't in a null zone
                {
                    if (NullZones.instance)
                        foreach (Null n in NullZones.instance.nulls)
                            if (n.bounds.Contains(poly.bounds.center))
                                continue;
                    if (poly.IsTouchingLayers(LayerMask.GetMask("Null")))
                        continue;
                }

                //Make sure the local null zone isn't on player
                {
                    Player p = Player.instance;
                    int TIMEOUT = 1000;
                    int i = 0;
                    //while (OverlapsCircle(ghostN.position, new List<Transform>(ghostN.GetComponentsInChildren<Transform>()), p.Location, p.Size) && i++ < TIMEOUT)
                    while (OverlapsCircle(poly, p.Location, p.Size) && i++ < TIMEOUT)
                    {
                        yield return new WaitForSeconds(0.1f);
                    }
                    if (i >= TIMEOUT)
                        continue;
                }

                validSpot = true;
            }


            yield return null;
            ghostT.parent = blockContainer.transform;
            Align(ghostT);
            sr.material.color = color;
            SetLayerRecursively(ghostT, "Wall");
            ghostT.name = ghost.name;
            Destroy(ghostN.gameObject);

            Color xColor, yColor;
            xColor = xMat.color;
            yColor = yMat.color;
            const float TRANSITION_STEPS = 10;
            for (int i = 0; i <= TRANSITION_STEPS; i++)
            {
                sr.material.color = Color.Lerp(nullColor, color, i / TRANSITION_STEPS);
                xMat.color = Color.Lerp(xColor, Color.clear, i / TRANSITION_STEPS);
                yMat.color = Color.Lerp(yColor, Color.clear, i / TRANSITION_STEPS);
                yield return null;
            }
            yield return new WaitForSeconds(1);
            for (int i = 0; i <= TRANSITION_STEPS; i++)
            {
                xMat.color = Color.Lerp(Color.clear, xColor, i / TRANSITION_STEPS);
                yMat.color = Color.Lerp(Color.clear, yColor, i / TRANSITION_STEPS);
                xBound.transform.localPosition = Vector3.Lerp(xBound.transform.localPosition, Vector3.zero, i / TRANSITION_STEPS);
                yBound.transform.localPosition = Vector3.Lerp(yBound.transform.localPosition, Vector3.zero, i / TRANSITION_STEPS);
                yield return null;
            }
        }
    }

    //bool OverlapsCircle(PolygonCollider2D poly, Vector3 loc, float s)
    //bool OverlapsCircle(Vector3 origin, List<Transform> cs, Vector3 loc, float s)
    bool OverlapsCircle(PolygonCollider2D poly, Vector3 loc, float s)
    {
        return poly.IsTouchingLayers(LayerMask.GetMask("Player")) || poly.OverlapPoint(loc);
        /*
        bool overlaps = poly.OverlapPoint(loc);
        for (int i = 0; i <= 10; i++)
        {
            overlaps |= poly.OverlapPoint(loc + Vector3.up * Mathf.Cos(i / 10f * 2 * Mathf.PI) + Vector3.right * Mathf.Sin(i / 10f * 2 * Mathf.PI));
        }
        return overlaps;
        */
    }
    void Align(Transform t)
    {
        float PRECISION = 0.2f;
        t.localPosition = new Vector3(Mathf.Round(t.localPosition.x / PRECISION) * PRECISION, 0, Mathf.Round(t.localPosition.z / PRECISION) * PRECISION);
    }

    void SetLayerRecursively(Transform p, string l)
    {
        p.gameObject.layer = LayerMask.NameToLayer(l);
        foreach (Transform t in p)
            if (!t.name.Contains("Sprite"))
                SetLayerRecursively(t, l);
    }

    [SerializeField]
    List<BlockTypes> blocks = new List<BlockTypes>();
    private GameObject NextBlock()
    {
        if (blocks.Count == 0) return null;
        GameObject go = GetBlock(blocks[0]);
        blocks.RemoveAt(0);
        return go;
    }

    static Dictionary<BlockTypes, GameObject> blockDictionary = new Dictionary<BlockTypes, GameObject>();
    static GameObject GetBlock(BlockTypes b)
    {
        if (!blockDictionary.ContainsKey(b))
            blockDictionary.Add(b, Resources.Load("Prefabs/" + b.ToString()) as GameObject);
        return blockDictionary[b];
    }
}
