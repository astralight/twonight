﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class Collectible : Collidable {

    public override void Collide(Vector3 otherPosition)
    {
        if(canCollide(otherPosition))
        {
            collect();
            radius = 0;
        }
    }
    public abstract void collect();
}
