﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent (typeof(AudioSource))]

public class SoundManager : MonoBehaviour {
    public static SoundManager instance;
    public AudioSource source { get; private set; }
    void Awake()
    {
        instance = this;
        source = GetComponent<AudioSource>();
    }
    public void PlaySound(SoundTypes type)
    {
        Debug.Log(type+"");
        AudioClip clip;
        if (audioClips.ContainsKey(type))
            clip = audioClips[type];
        else
            return;

        source.PlayOneShot(clip);
    }
    Dictionary<SoundTypes, AudioClip> _audioClips;
    Dictionary<SoundTypes, AudioClip> audioClips {
        get
        {
            if(_audioClips == null)
            {
                _audioClips = new Dictionary<SoundTypes, AudioClip>();
                foreach (SoundTypes type in Enum.GetValues(typeof (SoundTypes)))
                {
                    _audioClips.Add(type, Resources.Load<AudioClip>("Audio/" + type.ToString()));
                }
            }
            return _audioClips;
        }
    }
    public enum SoundTypes
    {
        PlaceBlock, GotStar, GotCloud
    }

}
