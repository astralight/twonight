﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Collidable : MonoBehaviour {
    public float radius = 1;
    public float sqrRadius
    {
        get { return radius * radius; }
    }
    public virtual void register()
    {
        list.Add(this);
    }
    public virtual void unregister()
    {
        list.Remove(this);
    }
    public void Awake()
    {
        Init();
        register();
    }
    public void OnDestroy()
    {
        unregister();
    }
    public virtual void Init() { }
    public abstract void Collide(Vector3 otherPosition);

    public static List<Collidable> list = new List<Collidable>();

    public virtual bool canCollide(Vector3 otherPosition)
    {
        otherPosition.z = 0;
        Vector3 thisPosition = transform.position;
        thisPosition.z = otherPosition.z = 0;
        float sqrMag = (otherPosition - thisPosition).sqrMagnitude;
        return sqrMag < sqrRadius;
    }

    public virtual void StartCollide() { }
    public virtual void EndCollide() { }

    bool wasColliding = false;
    public virtual void Recheck()
    {
        if(canCollide(Player.instance.Location))
        {
            if(!wasColliding)
            {
                wasColliding = true;
                StartCollide();
            }
            Collide(Player.instance.Location);
        }
        else
        {
            if(wasColliding)
            {
                wasColliding = false;
                EndCollide();
            }
        }
    }
}
