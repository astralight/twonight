﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
namespace UI
{
    public class Stars : MonoBehaviour
    {

        public static Stars instance { get; private set; }
        void Awake()
        {
            instance = this;
            SetStars(0);
        }
        [SerializeField]
        List<Image> images; //AIE
        int stars = 0;
        public void SetStars(int stars)
        {
            this.stars = stars;
            for (int i = 0; i < Mathf.Min(3, images.Count); i++)
                images[i].color = (i < stars) ? Color.white : Color.gray ;
        }
        public void IncStars()
        {
            SetStars(stars + 1);
        }
    }
}
