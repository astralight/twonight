﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
namespace UI
{
    public class Blocks : MonoBehaviour
    {
        public static Blocks instance { get; private set; }
        void Awake()
        {
            instance = this;
            SetBlocks(new List<BlockTypes>());
        }
        [SerializeField]
        List<Image> images; //AIE
        Dictionary<BlockTypes, Sprite> dictionary = new Dictionary<BlockTypes, Sprite>();
        public void SetBlocks(List<BlockTypes> blockTypes)
        {
            StartCoroutine(SetBlocksIE(blockTypes));
        }
        public IEnumerator SetBlocksIE(List<BlockTypes> blockTypes)
        {
            Sprite s;
            for (int i = 0; i < images.Count; i++)
                if (images[i].enabled = (i < images.Count && i < blockTypes.Count))
                    if (TryGetBlock(blockTypes[i], out s))
                        images[i].sprite = s;
            yield return null;

        }
        public bool TryGetBlock(BlockTypes bt, out Sprite s)
        {
            s = null;
            if (!dictionary.ContainsKey(bt))
                dictionary.Add(bt, (Sprite)(Instantiate(Resources.Load("Blocks/"+bt, typeof(Sprite)))));
            if (dictionary[bt] == null)
                return false;
            s = dictionary[bt];
            return true;
        }
    }
}
