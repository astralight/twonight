﻿using UnityEngine;
using System.Collections;
using System;

public class Cannon : Collidable
{
    float cooldown = 1;
    public float maxCooldown = 1;
    public float power = 1;
    public void Update()
    {
        if (cooldown <= 0)
        {
        }
        else
        {
            cooldown -= Time.deltaTime;
        }
    }

    public override void Collide(Vector3 otherPosition)
    {
        if(canCollide(otherPosition) && cooldown <= 0)
        {
            cooldown = maxCooldown;
            //~todo
            Player.instance.OverridePath(transform.right);
        }
    }
}
