﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {
    public List<GameObject> prefab;

    public float nextTimeMin;
    public float nextTimeMax;

    public float nextTime = 0;
	void Update () {
	    if(Time.time > nextTime)
        {
            GameObject go = GameObject.Instantiate(prefab[Random.Range(0, prefab.Count)]);
            go.transform.position = transform.position;
            nextTime = Time.time + (Random.Range(nextTimeMin, nextTimeMax));
        }
	}
}
