﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controller : MonoBehaviour {
    
    // Use this for initialization
    void Begin() {
        forwardVector = transform.right;
        newRight = transform.right;
        StartCoroutine(_loop = Loop());
        StartCoroutine(_rotation = LoopRotation());
        StartCoroutine(_collides = LoopCollides());
        Player.instance = new Player(this);
    }
    [SerializeField] SpriteRenderer sprite;
    public void goToCloud(Cloud cloud)
    {
        StartCoroutine(goToCloudIE(cloud));
    }
    IEnumerator goToCloudIE(Cloud cloud)
    {
        float max;
        Vector3 playerPosition = transform.position;
        Vector3 cloudPosition = cloud.transform.position + cloud.transform.forward * 0.5f; 
        //sprite.transform.forward = cloud.transform.forward; //align
        StopCoroutine(_loop);
        StopCoroutine(_rotation);
        StopCoroutine(_collides);
        for (int i = 0; i < (max = 30 * 2); i++)
        {
            transform.position = Vector3.Lerp(playerPosition, cloudPosition, i / max);
            transform.rotation = Quaternion.Lerp(transform.rotation, cloud.transform.rotation, i / max);
            yield return null;
        }
        transform.position = cloudPosition;
        while (!InputManager.instance.tryEatInput())
            yield return null;
        LevelController.instance.GoToNextLevel();
    }

    [SerializeField] Vector3 _forwardVector;
    const float INCREMENTS = 10;
    [SerializeField]
    Vector3 newRight;
    public Vector3 forwardVector
    { 
        get
        {
            return _forwardVector;
        }
        set
        {
            _forwardVector = value;
            searchVectors.Clear();
            for (int i = 0; i <= INCREMENTS * 1.5f; i++)
            {
                Vector3 v;
                if (i <= INCREMENTS)
                    v = Vector3.LerpUnclamped(forwardVector, Vector3.Cross(forwardVector, Vector3.forward), i / INCREMENTS);
                else
                    v = Vector3.Lerp(Vector3.Cross(forwardVector, Vector3.forward), -forwardVector, (i-INCREMENTS)/INCREMENTS);
                searchVectors.Add(v);
            }
            newRight = value;
        }
	}
    List<Vector3> searchVectors = new List<Vector3>();
    [SerializeField] Transform target = null;
    [SerializeField] float searchWidth;
    [SerializeField]
    float[] searchWidthes;
    Vector3 lastPosition = Vector3.zero;
    IEnumerator LoopRotation()
    {
        forwardVector = newRight = transform.right;
        yield return null;
        //Some weird stuff going on here. Sorry! I think I shouldve just used Quaternions?
        while (true)
        {
            //Get the current angle theta.
            float ctheta = transform.localEulerAngles.y;
            //Calculate the new angle theta. For whatever reason, it's 360 - [the value]. Hierarchy I guess.
            float ntheta = 360f - Mathf.Atan2(newRight.y, newRight.x) * Mathf.Rad2Deg;

            float delta = (ctheta - ntheta);
            //ntheta is [0, 360] and so is ctheta.
            // however, if ctheta = 0 and ntheta = 359, we want ctheta to go BACKwards to -1 (delta = -1) instead of forwards to 359 (delta = 359) because the abs of delta is lower.
            if (Mathf.Abs(ctheta - (ntheta-360f)) < Mathf.Abs(ctheta - ntheta))
                ntheta -= 360f;
            //recalculate delta with new ntheta.
            delta = (ctheta - ntheta);

            transform.localEulerAngles = Vector3.MoveTowards(Vector3.up * ctheta, Vector3.up * ntheta, Mathf.Abs(delta) * Time.deltaTime * 5);

            yield return null;
        }

    }
    IEnumerator LoopCollides()
    {
        while (true)
        {
            foreach(Collidable c in Collidable.list)
            {
                if(c.canCollide(transform.position))
                {
                    c.Collide(transform.position);
                }
            }
            yield return null;
        }

    }
    public IEnumerator _loop;
    public IEnumerator _collides;
    public IEnumerator _rotation;
    public bool searchRestriction
    {
        get;
        set;
    }
    public IEnumerator Loop ()
    {
        while (true)
        {
            List<Vector3> oSearchVectors = new List<Vector3>(searchVectors);
            Transform ttarget = null;
            for(int i = 0; i < (searchRestriction ? 1 : oSearchVectors.Count); i++)
            //foreach (Vector3 v in oSearchVectors)
            {
                Vector3 v = oSearchVectors[i];
                RaycastHit2D rh;
                Debug.DrawLine(transform.position, transform.position + v * searchWidth, Color.Lerp(Color.blue, Color.green, i++ / INCREMENTS), 1);
                if ((rh = Physics2D.Raycast(transform.position, v, searchWidth, 1 << 8)) && rh.transform.gameObject.layer == LayerMask.NameToLayer("Wall"))
                {
                    //foreach (RectTransform rt in rh.transform.GetComponents<RectTransform>())
                    Debug.DrawLine(rh.transform.position, rh.transform.position + rh.transform.right*1, Color.cyan, 1);


                    searchRestriction = false;
                    //Debug.Log(rh.transform.name);
                    Transform t = rh.transform;
                    forwardVector = t.right;
                    ttarget = target = t;
                    Vector3 p = transform.position;
                    //Debug.DrawLine(rh.point, (Vector3)rh.point + t.forward, Color.grey, 1);
                    Debug.DrawLine(rh.point, (Vector3)rh.point + t.up, Color.grey, 10);
                    //Debug.DrawLine(rh.point, (Vector3)rh.point + t.right, Color.grey, 1);
                    Vector3 p2 = (Vector3)rh.point + t.up * DISTANCE_OFF_GROUND + t.right * FORWARD_MOVEMENT;
                    
                    //I still have no idea what this does
                    //if (!(((Vector3)rh.point + t.right * CLIFF_SIGMA - transform.position).magnitude < ((Vector3)rh.point - t.right * CLIFF_SIGMA - transform.position).magnitude))
                    {
                        p2 += t.right * CLIFF_EDGE_LEAP;
                        for (int j = 0; j <= 10f; j++)
                        {
                            if ((transform.position - p2).magnitude < CLIFF_EDGE_DETECTION)
                            {
                                Debug.Log("Cliff Edge Detection");
                                transform.position = p2;
                                break;
                            }
                            Vector3 d = Vector3.Lerp(p, p2, j / 10f);
                            transform.position = d;
                            Debug.DrawLine(transform.position, transform.position + v * searchWidth, Color.Lerp(Color.red, Color.clear, 0.3f), 1);
                            yield return new WaitForSeconds(0.01f);
                        }
                    }
                    

                    transform.position = p2;
                    break;
                }
            }
            if (ttarget == null)
            {
                Debug.Log("Null Target");
                target = null;
                transform.Translate(newRight * REGULAR_LEAP, Space.World);
            }
            else
            {
                searchWidth = target == null ? searchWidthes[0] : searchWidthes[1];
            }
            //Debug.Log((transform.position - lastPosition).magnitude);
            if ((transform.position - lastPosition).magnitude < BUGOUT_DETECTION)
            {
                Debug.Log("Bugout");
                Debug.DrawLine(transform.position, transform.position + forwardVector * Mathf.Min(BUGOUT_LEAP,1), Color.yellow, 1);
                transform.Translate((Random.value > 0.7f ? -1 : 1 ) * forwardVector * BUGOUT_LEAP * Mathf.Max(Random.value,0.8f), Space.World);
            }
            lastPosition = transform.position;
            yield return null;
        }
	}
    const float CLIFF_SIGMA = 0.001f;
    const float DISTANCE_OFF_GROUND = 0.1f;
    const float CLIFF_EDGE_DETECTION = 0.01f;
    const float CLIFF_EDGE_LEAP = 0.075f;
    const float BUGOUT_DETECTION = 0.002f;
    const float BUGOUT_LEAP = 0.1f;
    const float REGULAR_LEAP = 0.1f;
    const float FORWARD_MOVEMENT = 0.1f;

}
