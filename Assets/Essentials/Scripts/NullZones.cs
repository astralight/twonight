﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NullZones : MonoBehaviour {
    public static NullZones instance;
    public void OnEnable()
    {
        instance = this;
    }
    void OnDisable()
    {
        instance = null;
    }
    public List<Null> nulls = new List<Null>();
	public void register(Null n)
    {
        nulls.Add(n);
    }
}
