﻿using UnityEngine;
using System.Collections;
using System;

public class WindyZones : BoundedCollidable
{
    Vector3 playerDirection;
    public Vector3 direction;
    public override void Collide(Vector3 otherPosition)
    {
        currentStrength = Vector3.MoveTowards(currentStrength, direction, Time.deltaTime);
        Player.instance.SetSecondaryVelocity("Wind", currentStrength);
    }
    Vector3 currentStrength;
    public override void StartCollide()
    {
        currentStrength = Vector3.zero;
    }
    public override void EndCollide()
    {
        currentStrength = Vector3.zero;
    }
    

}
