﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelController : MonoBehaviour {
    private Camera _camera;
    Camera camera { get { return _camera == null ? _camera = Camera.main : _camera; } }
    [SerializeField]
    List<GameObject> levels = new List<GameObject>();
    private int level_ = 3;
    public int level
    {
        get
        {
            return level_;
        }
        set
        {
            InputManager.instance.tryEatInput(); //Make sure input doesnt carry over from last level.
            UI.Stars.instance.SetStars(0);
            for (int i = 0; i < levels.Count; i++)
                if(levels[i] != null)
                {
                    levels[i].SetActive(i == value);
                    if (i == value)
                        levels[i].BroadcastMessage("Begin");
                }
            level_ = value;
            StartCoroutine(MoveCamera(levels[level_]));
        }
    }
    public void GoToNextLevel() { level++; } //todo: change it to go to next chapter if done.
    IEnumerator MoveCamera(GameObject level)
    {
        Vector3 oldP, newP;
        oldP = camera.transform.position;
        newP = level.transform.position;
        newP.z = oldP.z;
        int CAMERA_MOVE_STEPS = 20;
        for(int i = 0; i <= CAMERA_MOVE_STEPS; i++)
        {
            camera.transform.position = Vector3.Lerp(oldP, newP, i / (float)CAMERA_MOVE_STEPS);
            yield return null;
        }
    }
    public static LevelController instance { get; private set; }
    void Awake()
    {
        instance = this;
    }
	void Start () {
        level = level;
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Period))
            level++;
    }
}
