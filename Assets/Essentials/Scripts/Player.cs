﻿using UnityEngine;
using System.Collections;
using System;

public class Player
{
    [SerializeField]
    public float Size
    {
        get; private set;
    }
    Controller controller;
    public Player(Controller c)
    {
        Size = 1;
        controller = c;
    }
    public static Player instance;
    public Vector3 Location{ get { return controller.transform.position; } }
    public void OverridePath(Vector3 dir)
    {
        controller.StartCoroutine(OverridePathIE(dir));
    }
    IEnumerator OverridePathIE(Vector3 dir)
    {
        //controller.StopCoroutine(controller._loop);
        controller.forwardVector = dir;
        yield return null;
        controller.searchRestriction = true;
    }
    public void GoToCloud(Cloud cloud)
    {
        controller.goToCloud(cloud);
    }
    public void SetSecondaryVelocity(String name, Vector3 dir)
    {
        throw new NotImplementedException();
    }
    internal bool Disable()
    {
        throw new NotImplementedException();
    }
}