﻿using UnityEngine;
using System.Collections;
using System;

public class Cloud : Collectible
{
    public override void collect()
    {
        SoundManager.instance.PlaySound(SoundManager.SoundTypes.GotCloud);
        Player.instance.GoToCloud(this);
    }
}
