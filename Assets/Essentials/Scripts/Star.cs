﻿using UnityEngine;
using System.Collections;
using System;

public class Star : Collectible
{
    bool collected = false;
    public override void collect()
    {
        if(collected || (collected = (collected == false)))
        {
            SoundManager.instance.PlaySound(SoundManager.SoundTypes.GotStar);
            StartCoroutine(FadeOut());
            UI.Stars.instance.IncStars();
        }
    }
    public IEnumerator FadeOut()
    {
        Player p = Player.instance;
        Vector3 v = transform.localScale;
        for (int i = 0; i <= 30*5; i++)
        {
            transform.position = Vector3.Lerp(transform.position, p.Location, i / (30 * 5f));
            transform.Rotate(Vector3.forward);
            transform.localScale = Vector3.Lerp(v, Vector3.zero, i/(30*5f));
            yield return null;
        }
        Destroy(gameObject);
    }
}
