﻿using UnityEngine;
using System.Collections;

public class Walls : MonoBehaviour {

	// Use this for initialization
	void Start () {
        int i = 0;
	    foreach(Transform t in transform)
            t.Translate(-Vector3.up*0.001f*i++);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
